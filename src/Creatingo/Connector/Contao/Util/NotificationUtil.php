<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Contao\Util;


use Creatingo\Connector\Client\Response\Notification;

class NotificationUtil
{
	/**
	 * @param Notification $notification
	 */
	public static function convertToMessage(Notification $notification)
	{
		$message = static::translateMessage($notification);
		$type    = static::getMessageType($notification);

		\Message::add($message, $type);
	}


	/**
	 * @param Notification $notification
	 * @return string
	 */
	public static function translateMessage(Notification $notification)
	{
		\System::loadLanguageFile('creatingo_client');

		$message = sprintf(
			$GLOBALS['TL_LANG']['creatingo_client'][$notification->getMessage()],
			$notification->getData()
		);

		return $message;
	}


	/**
	 * @param Notification $notification
	 * @return string
	 */
	public static function getMessageType(Notification $notification)
	{
		switch($notification->getType()) {
			case Notification::ERROR:
				return 'TL_ERROR';
				break;

			case Notification::SUCCESS:
				return 'TL_SUCCESS';
				break;

			default:
				return 'TL_INFO';
				break;
		}
	}

} 
