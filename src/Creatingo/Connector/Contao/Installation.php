<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Contao;

use Creatingo\Connector\Client\Domain\Item;
use Creatingo\Connector\Client\Installation\Installation as FilesystemInterface;
use Creatingo\Connector\Client\Installation\InstalledItem;
use Creatingo\Connector\Client\Installation\InstalledItemCollection;

class Installation implements FilesystemInterface
{
	/**
	 * @var string
	 */
	private $baseDir;

	/**
	 * @var InstalledItemCollection
	 */
	private $installed;


	/**
	 * @param $baseDir
	 */
	function __construct($baseDir)
	{
		$this->baseDir = $baseDir;
	}


	/**
	 * @param string $baseDir
	 */
	public function setBaseDir($baseDir)
	{
		$this->baseDir = $baseDir;
	}


	/**
	 * @return string
	 */
	public function getBaseDir()
	{
		return $this->baseDir;
	}


	/**
	 * @param Item $item
	 * @param $content
	 * @param bool $force
	 * @return bool
	 */
	public function installItem(Item $item, $content, $force=false)
	{
		if($this->getInstalledItems()->isInstalled($item)) {
			if(!$force) {
				return true;
			}

			$installed = $this->getInstalledItems()->getInstalledItem($item);
			$path      = $installed->get('path');

			$files = new \Folder($path);
			$files->delete();
		}

		$path = $this->baseDir . '/' . $item->getName();
		$name = md5($item->getName() . time());

		$file = new \File('system/temp/' . $name);
		$file->write($content);
		$file->close();

		$zip  = new \ZipReader('system/temp/' . $name);

		while($zip->next()) {
			$unzippedFile = new \File($path . '/' . $zip->file_name);
			$unzippedFile->write($zip->unzip());
			$unzippedFile->close();
		}

		$file->delete();
		\Dbafs::addResource($path);

		$model = \FilesModel::findOneBy('path', $path);
		$model->creatingo_id = $item->getId();
		$model->save();

		return true;
	}


	/**
	 * @param bool $force
	 * @return InstalledItemCollection
	 */
	public function getInstalledItems($force=false)
	{
		if(!$this->installed || $force) {
			$collection = new InstalledItemCollection();
			$result     = \Database::getInstance()
				->prepare('SELECT path, id, uuid, creatingo_id FROM tl_files WHERE creatingo_id !=?')
				->execute('');

			while($result->next()) {
				$installed = new InstalledItem($result->creatingo_id, $result->row());
				$collection->addItem($installed);
			}

			$this->installed = $collection;
		}

		return $this->installed;
	}

} 
