<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Contao;


use Creatingo\Connector\Client\Api\Client;
use Creatingo\Connector\Client\Controller\ItemController;
use Creatingo\Connector\Client\Response\Notification;
use Creatingo\Connector\Client\Response\Template;
use Creatingo\Connector\Contao\Controller\CreditsController;
use Creatingo\Connector\Contao\Controller\SetupController;
use Creatingo\Connector\Contao\Util\NotificationUtil;
use Guzzle\Http\Exception\BadResponseException;

class BackendModule extends \Backend
{

	/**
	 * @var array
	 */
	private $options;

	/**
	 * @var Client
	 */
	private $client;


	/**
	 *
	 */
	protected function __construct()
	{
		parent::__construct();

		$this->options = include TL_ROOT . '/system/modules/creatingo-connector/config/creatingo.php';

		$this->loadLanguageFile('creatingo_connector');

		$GLOBALS['TL_CSS'][] = 'system/modules/creatingo-connector/assets/css/style.css';
		$GLOBALS['TL_CSS'][] = 'assets/mootools/slimbox/1.8/css/slimbox.css';
		$GLOBALS['TL_JAVASCRIPT'][] = 'assets/mootools/slimbox/1.8/js/slimbox.js';
		$GLOBALS['TL_MOOTOOLS'][]   = "<script> (function($){window.addEvent('domready',function(){ $$(document.links).filter(function(el){return el.getAttribute('data-lightbox')!= null;}).slimbox({},null,function(el){var attr = this.getAttribute('data-lightbox');return(this == el)||(attr && el.getAttribute('data-lightbox').match(attr));});$('lbImage').addEvent('swipe',function(e){(e.direction == 'left') ? $('lbNextLink').fireEvent('click'):$('lbPrevLink').fireEvent('click');});});})(document.id);</script>";
	}


	/**
	 * @return string
	 */
	public function generate()
	{
		try {
			$response = $this->getResponse();
		}
		catch(BadResponseException $e) {
			$status = $e->getResponse()->getStatusCode();

			if($status == 403 || $status == 401) {
				$controller = new SetupController($this->client);

				$response = $controller->setupToken();
			}
		}
		catch(\Exception $e) {
			$this->log('Invalid response in creatingo cloud client:' . $e->getMessage(), __METHOD__, TL_ERROR);
			$this->redirect('contao/main.php?act=error');

			exit;
		}

		if(is_string($response)) {
			return $response;
		}

		elseif($response instanceof Notification) {
			NotificationUtil::convertToMessage($response);
			$this->redirect($this->getReferer());
		}

		elseif($response instanceof Template) {
			$data = $response->getData();
			$data['requestToken'] = \RequestToken::get();
			$data['action']		  = 'contao/main.php?do=creatingo&ref=' . \Input::get('ref');

			$template = new \BackendTemplate($response->getTemplate(), $response->getContentType());
			$template->setData($data);

			return $template->parse();
		}

		$this->log('Invalid response in creatingo cloud client.', __METHOD__, TL_ERROR);
		$this->redirect('contao/main.php?act=error');

		exit;
	}


	/**
	 * @return bool
	 */
	private function isTokenGiven()
	{
		if(!$this->getConfig('token')) {
			return false;
		}

		return true;
	}


	/**
	 * @return bool
	 */
	private function isTargetChosen()
	{
		if(!$this->getConfig('target')) {
			return false;
		}

		return true;
	}


	/**
	 * Make sure that chosen target exists
	 */
	private function guardTargetExists()
	{
		$target = \FilesModel::findOneBy('path', $this->getConfig('target'));

		if(!$target) {
			\Message::add(sprintf(
					'Invalid target given. Target "%s" does not exists.',
					$this->getConfig('target')
				),
				'TL_ERROR'
			);

			$config = \Config::getInstance();
			$config->add("\$GLOBALS['TL_CONFIG']['creatingo_target']", null);
			$config->save();

			\Controller::reload();
		}
	}


	/**
	 * @throws \Exception
	 * @return Notification|Template|string
	 */
	private function getResponse()
	{
		$client   = $this->getClient();
		$response = $this->checkInstallation();

		if($response) {
			return $response;
		}

		if(\Input::get('credits')) {
			$controller = new CreditsController($client);
			return $controller->execute();
		}

		$this->guardTargetExists();

		$installation = new Installation($this->getConfig('target'));
		$controller   = new ItemController($client->items(), $installation);
		$id           = \Input::get('download');

		if($id) {
			return $controller->downloadItem($id);
		}

		$keyword  = \Input::post('keyword');
		$category = \Input::post('category');
		$color    = \Input::post('color');
		$page     = \Input::post('page');
		$type     = \Input::post('type');

		$data = array(
			'creditsLink' => $this->addToUrl('credits=1')
		);

		$response = $controller->listItems($category, $keyword, $color, $type, $page);
		$response->addData($GLOBALS['TL_LANG']['creatingo_connector']['list']);
		$response->addData($data);

		return $response;
	}


	/**
	 * @param $name
	 * @return null
	 */
	protected function getConfig($name)
	{
		if(isset($GLOBALS['TL_CONFIG']['creatingo_' . $name])) {
			return $GLOBALS['TL_CONFIG']['creatingo_' . $name];
		}

		return null;
	}


	/**
	 * @return Client
	 */
	private function getClient()
	{
		if(!$this->client) {
			$this->client = Client::connect(
				$this->options['base_uri'] . '/' . $this->options['api_version'],
				$this->getConfig('token'),
				array(
					'CREATINGO_CONNECTOR'   => $this->options['connector'],
					'CREATINGO_API_VERSION' => $this->options['api_version'],
					'Accept-Language'       => $GLOBALS['TL_LANGUAGE']
				)
			);
		}

		return $this->client;
	}


	/**
	 * @return string|null
	 */
	private function checkInstallation()
	{
		if(!$this->isTokenGiven()) {
			$controller = new SetupController($this->client);

			return $controller->setupToken();
		}

		if(!$this->isTargetChosen()) {
			$controller = new SetupController($this->client);

			return $controller->chooseTarget();
		}

		return null;
	}

}
