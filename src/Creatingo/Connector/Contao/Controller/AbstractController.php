<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Contao\Controller;


abstract class AbstractController extends \Controller
{
	/**
	 * @return array
	 */
	protected function getDefaultData()
	{
		$referrer = new \stdClass();
		$referrer->href   = \Controller::getReferer(true);
		$referrer->title  = $GLOBALS['TL_LANG']['MSC']['backBTTitle'];
		$referrer->button = $GLOBALS['TL_LANG']['MSC']['backBT'];

		return array(
			'referrer' => $referrer,
			'action'   => \Environment::get('request'),
			'messages' => \Message::generate()
		);
	}
}
