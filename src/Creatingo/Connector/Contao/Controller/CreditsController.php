<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Contao\Controller;


use Creatingo\Connector\Client\Api\Client;
use Creatingo\Connector\Client\Response\Template;

class CreditsController extends AbstractController
{
	/**
	 * @var Client
	 */
	private $client;


	/**
	 * @param $client
	 */
	function __construct(Client $client)
	{
		$this->client = $client;
	}


	/**
	 * @return Template
	 */
	public function execute()
	{
		$this->loadLanguageFile('creatingo_connector');

		$data = $this->getDefaultData();
		$data = array_merge($data, $GLOBALS['TL_LANG']['creatingo_connector']['credits']);

		$data['account'] = $this->client->account();

		return new Template('creatingo_credits', $data);
	}

} 
