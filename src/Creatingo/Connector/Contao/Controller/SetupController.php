<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

namespace Creatingo\Connector\Contao\Controller;


use Creatingo\Connector\Client\Api\Client;
use Creatingo\Connector\Client\Response\Template;

class SetupController extends AbstractController
{
	/**
	 * @var Client
	 */
	private $client;


	/**
	 * @param Client $client
	 */
	function __construct(Client $client)
	{
		$this->client = $client;

		$this->loadLanguageFile('creatingo_connector');
	}


	/**
	 * @return string
	 */
	public function setupToken()
	{

		if(\Input::post('FORM_SUBMIT') == 'creatingo_setup') {
			$token = \Input::post('token');

			$config = \Config::getInstance();
			$config->add("\$GLOBALS['TL_CONFIG']['creatingo_token']", (string)$token);
			$config->save();

			$this->reload();
		}

		$data = $this->getDefaultData();
		$data = array_merge($data, (array) $GLOBALS['TL_LANG']['creatingo_connector']['setup']);

		return new Template('creatingo_setup_token', $data);
	}


	/**
	 * @return string
	 */
	public function chooseTarget()
	{

		if(\Input::post('FORM_SUBMIT') == 'creatingo_target') {
			$target = \Input::post('target');

			$config = \Config::getInstance();
			$config->add("\$GLOBALS['TL_CONFIG']['creatingo_target']", $target);
			$config->save();

			$this->reload();
		}

		$this->loadDataContainer('tl_settings');
		$config = \Widget::getAttributesFromDca(
			$GLOBALS['TL_DCA']['tl_settings']['fields']['creatingo_target'],
			'target',
			null,
			'creatingo_target',
			'tl_settings'
		);

		$widget = new \FileSelector($config);

		$data = $this->getDefaultData();
		$data = array_merge($data, (array) $GLOBALS['TL_LANG']['creatingo_connector']['setup_target']);
		$data['targetPicker'] = $widget->generate();

		return new Template('creatingo_setup_target', $data);
	}

}
