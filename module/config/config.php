<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

array_insert($GLOBALS['BE_MOD']['system'], 1, array(
	'creatingo' => array(
		'label'    => $GLOBALS['TL_LANG']['MOD']['creatingo'],
		'callback' => 'Creatingo\Connector\Contao\BackendModule',
		'icon'     => 'system/modules/creatingo-connector/assets/img/icon.png',
	),
));
