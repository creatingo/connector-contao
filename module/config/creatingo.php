<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

return array(
	'base_uri'    => 'https://www.creatingo.com/system/modules/creatingo-server/api/app.php',
	'api_version' => 'v1',
	'connector'   => 'contao'
);
