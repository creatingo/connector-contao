<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

\TemplateLoader::addFiles(array(
	'creatingo_credits'      => 'system/modules/creatingo-connector/templates',
	'creatingo_list'         => 'system/modules/creatingo-connector/templates',
	'creatingo_setup_token'  => 'system/modules/creatingo-connector/templates',
	'creatingo_setup_target' => 'system/modules/creatingo-connector/templates',
));
