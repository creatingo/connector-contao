<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

$GLOBALS['TL_LANG']['creatingo_connector']['setup']['headline']    = 'Willkommen bei Creatingo Image Stock';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['subheadline'] = 'Bekomme Bilder für deine Seite 3 einfachen Schritten:';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['description'] = '<ol>
<li>Erstelle einen kostenlosen Account auf <a href="http://www.creatingo.com" target="_blank">www.creatingo.com</a></li>
<li>Kopiere den Autentifizierungs-Token und führe die Verifizierung druch</li>
<li>Bestimme den Download-Ordner</li>
</ol>

<p>Nach der erfolgreichen Verizifierung können Bilder nun bequem heruntergeladen werden.</p>';

$GLOBALS['TL_LANG']['creatingo_connector']['setup']['token_field'][0]   = 'Dein Autentifizierungs-Token';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['token_field'][1]   = 'Bitte gibt deinen Autentifizierungs-Token ein';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['run_verification'] = 'Verifizierung starten';

$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['headline']     = 'Willkommen bei Creatingo Image Stock';
$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['subheadline']  = 'Downloadverzeichnis';
$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['description']  = 'Wähle dein Downloadverzeichnis';
$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['submitButton'] = 'Speichern und Image Stock verwenden';

$GLOBALS['TL_LANG']['creatingo_connector']['list']['categoryLabel']     = 'Kategorie';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['colorLabel']        = 'Farbe';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['searchLabel']       = 'Suche';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['searchPlaceholder'] = 'Suchwort';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['fileTypeLabel']     = 'Dateityp';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['downloadLabel']     = 'Herunterladen';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['installedLabel']    = 'Zur Dateiverwaltung';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['sizeLabel']         = 'Größe';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['free']              = 'Nur Freie';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['premium']           = 'Nur Premium';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['all']               = 'Alle Einträge';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['creditsLabel']      = 'Informationen';

$GLOBALS['TL_LANG']['creatingo_connector']['credits']['headline']               = 'Informationen';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['accountHeadline']        = 'Account Details';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['creatingoHeadline']      = 'Creatingo Image Stock';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['creatingoDescription']   = '<a href="http://www.creatingo.com" target="_blank">Creatingo</a> bietet hilfreiche Bilder (Icons, Hintergründe, Grafiken, Fotos) für die tägliche Arbeite an. Inhalte aus erster Hand, tägliche Updates.';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['connectorHeadline']      = 'Creatingo Image Stock Connector';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['connectorDescription']   = 'Der Creatingo Image Stock Connector wird von <a href="http://www.netzmacht.de" target="_blank">netzmacht creative</a> entwickelt.';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['accountType']['premium'] = 'PREMIUM Mitgliedschaft';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['accountType']['free']    = 'FREE Mitgliedschaft';
