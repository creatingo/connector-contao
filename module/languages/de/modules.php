<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

$GLOBALS['TL_LANG']['MOD']['creatingo'][0] = 'Image Stock';
$GLOBALS['TL_LANG']['MOD']['creatingo'][1] = 'Creatingo Image Stock';
