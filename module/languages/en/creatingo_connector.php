<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

$GLOBALS['TL_LANG']['creatingo_connector']['setup']['headline']    = 'Welcome to the Creatingo Image Stock';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['subheadline'] = 'Get images for your site in 3 easy steps:';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['description'] = '<ol>
<li>Create your free account at <a href="http://www.creatingo.com" target="_blank">www.creatingo.com</a></li>
<li>Paste authentification token and run verification</li>
<li>Choose download target.</li>
</ol>

<p>After successful verification you can download files directly to your server.</p>';

$GLOBALS['TL_LANG']['creatingo_connector']['setup']['token_field'][0]   = 'Your authentification token';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['token_field'][1]   = 'Please insert your authentification token';
$GLOBALS['TL_LANG']['creatingo_connector']['setup']['run_verification'] = 'Run verification';

$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['headline']     = 'Welcome to the Creatingo Image Stock';
$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['subheadline']  = 'Download target';
$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['description']  = 'Choose your download target';
$GLOBALS['TL_LANG']['creatingo_connector']['setup_target']['submitButton'] = 'Save and use Image Stock';

$GLOBALS['TL_LANG']['creatingo_connector']['list']['categoryLabel']     = 'Category';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['colorLabel']        = 'Color';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['searchLabel']       = 'Search';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['searchPlaceholder'] = 'Search keyword';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['fileTypeLabel']     = 'File type';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['downloadLabel']     = 'Download';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['installedLabel']    = 'Downloaded';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['sizeLabel']         = 'Size';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['free']              = 'Free only';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['premium']           = 'Premium only';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['all']               = 'All items';
$GLOBALS['TL_LANG']['creatingo_connector']['list']['creditsLabel']      = 'Informations';

$GLOBALS['TL_LANG']['creatingo_connector']['credits']['headline']               = 'Informations';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['accountHeadline']        = 'Account details';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['creatingoHeadline']      = 'Creatingo Image Stock';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['creatingoDescription']   = '<a href="http://www.creatingo.com" target="_blank">Creatingo</a> provides handy stuff (icons, backgrounds, photos, pictures) for everyday work. Original content, daily updates.';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['connectorHeadline']      = 'Creatingo Image Stock Connector';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['connectorDescription']   = 'Creatingo Image Stock Connector is developed by <a href="http://www.netzmacht.de" target="_blank">netzmacht creative</a>.';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['accountType']['premium'] = 'PREMIUM membership';
$GLOBALS['TL_LANG']['creatingo_connector']['credits']['accountType']['free']    = 'FREE membership';
