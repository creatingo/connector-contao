<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */
$GLOBALS['TL_DCA']['tl_files']['fields']['creatingo_id'] = array
(
	'sql' => "int(10) unsigned NOT NULL default '0'"
);
