<?php

/**
 * This file is part of the Creatingo Stock Image connector.
 *
 * @package    cloud-connector
 * @author     David Molineus <david.molineus@netzmacht.de>
 * @copyright  2014 netzmacht creative David Molineus
 * @license    CC BY-NC-ND 4.0
 *
 */

$GLOBALS['TL_DCA']['tl_settings']['fields']['creatingo_target'] = array(
	'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['creatingo_target'],
	'exclude'                 => true,
	'inputType'               => 'fileTree',
	'eval'                    => array('files'=>false, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr'),
);
